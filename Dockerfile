# base image
FROM node:lts-alpine AS runtime

LABEL maintainer="Gautam Sathe <gautam@hmsnz.com>"

WORKDIR /app

COPY . .

RUN npm install

ENV HOST=0.0.0.0
ENV PORT=4321
EXPOSE 4321

CMD ["npm", "run", "dev", "--", "--host", "0.0.0.0"]
